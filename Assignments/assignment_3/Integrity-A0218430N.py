# !/usr/bin/env python3
import os
import sys
from Cryptodome.Hash import SHA256

if len(sys.argv) < 3:
    print("Usage: python3 ", os.path.basename(__file__), "key_file_name document_file_name")
    sys.exit()

key_file_name   = sys.argv[1]
file_name       = sys.argv[2]

mac_key = bytes()
mac_from_file = bytes()
mac_generated = bytes()

with open(key_file_name, "rb") as f_in:
    mac_key = f_in.read(32)

with open(file_name, "rb") as f_in:
    file_data = bytes()
    while True:
        d = f_in.read()
        if len(d) == 0:
            break
        file_data += d

    mac_from_file = file_data[:32]
    mac_generated = SHA256.new(file_data[32:] + mac_key).digest()

if mac_from_file == mac_generated:
    print ('yes')
else:
    print ('no')