import os.path
import sys
from Cryptodome.Random import get_random_bytes
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import PKCS1_v1_5, AES
from socket import socket, AF_INET, SOCK_STREAM

rsa_key_len = int(2048/8)


def load_rsa_key(f_name_key="test/rsa_key.bin"):
    return RSA.importKey(open(f_name_key, "rb").read())

# Connect to the server
if len(sys.argv) < 5:
    print ("Usage: python3 ", os.path.basename(__file__), "key_file_name data_file_name hostname/IP port")
else:
    key_file_name   = sys.argv[1]
    data_file_name  = sys.argv[2]
    serverName      = sys.argv[3]
    serverPort      = int(sys.argv[4])
    print (serverName, serverPort)

    rsa_key = load_rsa_key()

    # Create a TCP socket.
    client_socket = socket(AF_INET, SOCK_STREAM)
    # Connect to the given server IP & port number.
    client_socket.connect((serverName, serverPort))

    # First 256 bytes sent by the server is the RSA encrypted session key.
    cipher_rsa = PKCS1_v1_5.new(rsa_key)
    ciphertext_rsa = client_socket.recv(256)
    aes_key = cipher_rsa.decrypt(ciphertext_rsa, get_random_bytes(16))
    # Write the AES key to key_file_name.
    open(key_file_name, "wb").write(aes_key)

    # Receive data from the server.
    # Decrypt the data in blocks of size 16B.
    # Write file to data_file_name.
    cipher_aes = AES.new(aes_key, AES.MODE_ECB)
    with open(data_file_name, "wb") as f_out:
        while True:
            ciphertext_aes = client_socket.recv(16)
            if len(ciphertext_aes) == 0:
                break
            f_out.write(cipher_aes.decrypt(ciphertext_aes))
    
    client_socket.close()