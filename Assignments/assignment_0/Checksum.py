import sys
import zlib

def main(src):
    with open(str(src), "rb") as f:
        bytes = f.read()
    checksum = zlib.crc32(bytes)
    print(checksum)

if __name__ == "__main__":
    main(sys.argv[1])