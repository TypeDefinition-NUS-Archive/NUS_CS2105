import sys

def main():
    # Data arrives in random sizes.
    # If a packet has not fully arrived, we need to store it in a buffer until the entire packet arrived.
    buffer = bytes()
    while True:
        # Take in some bytes, but we don't know if it consists of an entire packet yet.
        data = sys.stdin.buffer.read1()
        if len(data) > 0:
            buffer += data

        # If the buffer is empty and there's no incoming data, terminate.
        if len(data) == 0 and len(buffer) == 0:
            break

        # Need to break into 2 loops, to prevent "batch processing" the data.
        # Process any completed packets currently in our buffer.
        while True:
            # Get header.
            header_start = buffer.find(b'Size: ')
            header_end = buffer.find(b'B')
            header_size = header_end - header_start + 1

            # Check if header is complete.
            if header_start < 0 or header_end < 0:
                break

            # Get body.
            body_size = int(buffer[header_start+6 : header_end].decode())
            body_start = header_end + 1
            body_end = body_start + body_size

            # Check if body is complete.
            if body_size + header_size > len(buffer):
                break
        
            # Get content.
            content = buffer[body_start : body_end]

            # Output to stdout.
            sys.stdout.buffer.write(content)
            sys.stdout.buffer.flush()

            # Remove handled data.
            buffer = buffer[body_end:]

if __name__ == "__main__":
    main()