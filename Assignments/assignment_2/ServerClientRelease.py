import os
import sys
import time
from _socket import SHUT_RDWR
from socket import socket, AF_INET, SOCK_STREAM

from IceHelperRelease import IceHelper

from queue import PriorityQueue

HEADER_SIZE = 16 # Checksum (8) + Sequence Number (4) + Data Length (4)
MAX_DATA_LENGTH = 1024 - HEADER_SIZE

FSM_PREP_STATE = 0
FSM_SEND_STATE = 1
FSM_RECV_STATE = 2
FSM_DONE_STATE = 3

class ServerClient:
    ice_helper          = IceHelper()
    packet_size_server  = ice_helper.sever_packet_size
    packet_size_client  = ice_helper.client_packet_size

    debug_level = 1

    def ice_debug(self, level, *arg):
        if level <= self.debug_level:
            for a in arg:
                print(a, end=' ')
            print()

    def ice_print(self, *arg):
        # ANSI colors
        _c = (
            "\033[0m",  # End of color
            "\033[36m",  # Cyan
            "\033[91m",  # Red
            "\033[35m",  # Magenta
        )

        if self.is_server:
            print(_c[1] + self.secret_student_id + ' Server:' + _c[0], end=' ')
        else:
            print(_c[2] + self.secret_student_id + ' Client:' + _c[0], end=' ')
        for a in arg:
            print(a, end=' ')
        print()

    def __init__(self, argv, is_server):
        if len(argv) < 6:
            print("missing command line parameter: ip_address port_number out_filename mode Student_id")
            self.ice_helper.print_unreliability_mode_info()
            exit(-1)

        i = 1
        self.ip_address          = str(sys.argv[i])
        i += 1
        self.port_number         = int(sys.argv[i])
        i += 1
        self.filename            = str(sys.argv[i])
        i += 1
        mode                     = int(sys.argv[i])
        i += 1
        self.secret_student_id   = str(sys.argv[i])

        self.is_server = is_server

        # open connection
        self.clientSocket = socket(AF_INET, SOCK_STREAM)
        self.clientSocket.connect((self.ip_address, self.port_number))

        # perform handshake
        self.handshake()

        # we can start transmitting the file now
        self.mode_error          = False
        self.mode_reorder        = False
        self.mode_reliable       = False
        if mode == 0:
            self.mode_reliable      = True
        elif mode == 1:
            self.mode_error         = True
        elif mode == 2:
            self.mode_reorder       = True
        else:
            print("ServerClient init: invalid mode")
            exit(-1)

    '''
    This is a complete code, no need to change the function!
    '''
    def handshake(self):
        # initial handshake is to pass the secret key
        # to which the server response with an ok
        # 'C' implies client
        message = 'STID_'
        if self.is_server:
            message += self.secret_student_id + '_' + 'S'
        else:
            message += self.secret_student_id + '_' + 'C'
        self.ice_print('sending: ' + message)
        self.clientSocket.sendall(message.encode())
        # wait to get a response '0_'
        self.ice_print("waiting for server response")
        while True:
            waiting_list_num = self.ice_helper.get_integer_from_socket(self.clientSocket)
            if waiting_list_num is None:
                exit(-1)
            self.ice_print("waiting_list_num: ", waiting_list_num)

            if waiting_list_num == 0:
                # we can start transmitting the file now
                break

    '''
    This is a complete code, no need to change the function!
    '''
    def send_file(self):
        print("Sending file...")
        if not self.is_server:
            self.ice_print("Client cannot send file")
            return

        if self.mode_reliable:
            self.send_file_reliable_channel()
        elif self.mode_reorder:
            self.send_file_reorder_channel()
        elif self.mode_error:
            self.send_file_error_channel()
        else:
            print("ServerClient send_file: invalid_mode")
            exit(-1)

    '''
    This is a complete code, no need to change the function!
    '''
    def recv_file(self):
        if self.is_server:
            self.ice_print("Server cannot receive file")
            return

        print("Receiving file...")

        # time taken estimated
        tic = time.time()

        if self.mode_reliable:
            self.recv_file_reliable_channel()
        elif self.mode_reorder:
            self.recv_file_reorder_channel()
        elif self.mode_error:
            self.recv_file_error_channel()
        else:
            print("ServerClient recv_file: invalid_mode")
            exit(-1)

        elapsed_time = time.time() - tic
        print("\n\n Elapsed time: ", elapsed_time, " Sec\n\n")

    def _fsm_prep(self, fsm, f_in):
        # Prepare a batch of packets.
        while len(fsm["buffer"]) < 64:
            # Pack data.
            data = f_in.read(MAX_DATA_LENGTH)
            data_length = len(data)
            data += b"\0" * (MAX_DATA_LENGTH - data_length) # Pad packet to 1024.

            # Generate checksum.
            packet = fsm["seq"].to_bytes(4, "big") + data_length.to_bytes(4, "big") + data
            checksum = 0
            for i in range(0, 254):
                checksum += (int.from_bytes(packet[i*4:(i+1)*4], "big") * (i % 4))
            checksum &= 0xffffffffffffffff # Fuck Python and it's weakly typed bullshit.

            # Add packet to send buffer.
            packet = checksum.to_bytes(8, "big") + packet
            ack = fsm["seq"] + max(1, data_length) # To make sure every ACK is unique.
            fsm["buffer"].append((fsm["seq"], ack, packet))
            fsm["seq"] = ack

            # Check if file complete.
            if data_length == 0:
                fsm["done"] = True
                break

        fsm["state"] = FSM_SEND_STATE

    def _fsm_send(self, fsm):
        # Send entire batch of packets.
        for p in fsm["buffer"]:
            self.clientSocket.send(p[2])
        fsm["state"] = FSM_RECV_STATE

    def _fsm_recv(self, fsm):
        # Receive responses.
        num_resp = len(fsm["buffer"])
        for i in range(0, num_resp):
            # Receive packet.
            packet = self.get_one_packet()

            # Remove ACKed from buffer if exists.
            ack = int.from_bytes(packet[0:4], "big")
            for x in fsm["buffer"]:
                if ack == x[1]:
                    fsm["buffer"].remove(x)
                    break

        if fsm["done"]:
            fsm["state"] = FSM_DONE_STATE if len(fsm["buffer"]) == 0 else FSM_SEND_STATE
        else:
            fsm["state"] = FSM_PREP_STATE

    '''
    File reception over Channel with packet errors
    '''
    def recv_file_error_channel(self):
        with open(self.filename, "wb") as f_out:
            done = False
            buffer = PriorityQueue()
            lowest_ack = 0
            while not done:
                # Receive message.
                recv_packet = self.get_one_packet()

                # Generate checksum.
                checksum = 0
                for i in range(0, 254):
                    j = i + 2
                    checksum += (int.from_bytes(recv_packet[j*4:(j+1)*4], "big") * (i % 4))
                checksum &= 0xffffffffffffffff # Fuck Python and it's weakly typed bullshit.

                # Send NAK if checksum fails.
                if checksum != int.from_bytes(recv_packet[0:8], "big"):
                    nak_packet = lowest_ack.to_bytes(4, "big") + (b"\0" * 60)
                    self.clientSocket.send(nak_packet)
                    continue

                # Received non-corrupt packet.
                data_length = int.from_bytes(recv_packet[12:16], "big")
                data = recv_packet[HEADER_SIZE:HEADER_SIZE + data_length]
                seq = int.from_bytes(recv_packet[8:12], "big")
                ack = seq + max(1, data_length)

                # Place packet into buffer.
                buffer.put((seq, ack, data))

                # Send ACK.
                ack_packet = ack.to_bytes(4, "big") + (b"\0" * 60)
                self.clientSocket.send(ack_packet)

                while not buffer.empty():
                    # Check if we have received the lowest ACK packet yet.
                    top = buffer.queue[0]
                    if lowest_ack != top[0]:
                        break

                    # Write data.
                    lowest_ack = top[1]
                    f_out.write(top[2])

                    # Check if file complete.
                    if len(top[2]) == 0:
                        done = True

                    # Remove packet from buffer.
                    buffer.get()

        print("[Success] Receive File OK")

    '''
    File transmission over Channel with packet errors
    '''
    def send_file_error_channel(self):
        file_size = os.path.getsize(self.filename)
        print("File Size: {}".format(file_size))
        with open(self.filename, "rb") as f_in:
            fsm = {"state" : FSM_PREP_STATE, "done" : False, "seq" : 0, "buffer" : []}
            while True:
                if fsm["state"] == FSM_PREP_STATE:
                    self._fsm_prep(fsm, f_in)
                elif fsm["state"] == FSM_SEND_STATE:
                    self._fsm_send(fsm)
                elif fsm["state"] == FSM_RECV_STATE:
                    self._fsm_recv(fsm)
                else:
                    break

        print("[Success] Send File OK")

    '''
    File reception over Channel which Reorders packets
    '''
    def recv_file_reorder_channel(self):
        with open(self.filename, "wb") as f_out:
            done = False
            buffer = PriorityQueue()
            lowest_ack = 0
            while not done:
                # Receive message.
                recv_packet = self.get_one_packet()

                # Receive packet.
                data_length = int.from_bytes(recv_packet[12:16], "big")
                data = recv_packet[HEADER_SIZE:HEADER_SIZE + data_length]
                seq = int.from_bytes(recv_packet[8:12], "big")
                ack = seq + max(1, data_length)

                # Place packet into buffer.
                buffer.put((seq, ack, data))

                while not buffer.empty():
                    # Check if we have received the lowest ACK packet yet.
                    top = buffer.queue[0]
                    if lowest_ack != top[0]:
                        break

                    # Write data.
                    lowest_ack = top[1]
                    f_out.write(top[2])

                    # Check if file complete.
                    if len(top[2]) == 0:
                        done = True

                    # Remove packet from buffer.
                    buffer.get()

        print("[Success] Receive File OK")

    '''
    File transmission over Channel which Reorders packets
    '''
    def send_file_reorder_channel(self):
        file_size = os.path.getsize(self.filename)
        print("File Size: {}".format(file_size))
        with open(self.filename, "rb") as f_in:
            seq = 0
            while True:
                # Pack data.
                data = f_in.read(MAX_DATA_LENGTH)
                data_length = len(data)
                data += b"\0" * (MAX_DATA_LENGTH - data_length) # Pad packet to 1024.

                # Generate dummy checksum. (Just to make all headers consistent between channels.)
                checksum = 0 & 0xffffffffffffffff

                # Send packet.
                packet = checksum.to_bytes(8, "big") + seq.to_bytes(4, "big") + data_length.to_bytes(4, "big") + data
                self.clientSocket.send(packet)

                # Update sequence number.
                seq += max(1, data_length) # Add 1 to make sure every ACK is unique, even if the data length is 0.

                # Check if file complete.
                if data_length == 0:
                    break

        print("[Success] File Send OK")

    '''
    File reception over Reliable Channel
    '''
    def recv_file_reliable_channel(self):
        with open(self.filename, "wb") as f_out:
            curr = 0
            while True:
                # Receive message.
                recv_packet = self.get_one_packet()

                # Extract data.
                data_length = int.from_bytes(recv_packet[12:16], "big")
                data = recv_packet[HEADER_SIZE:HEADER_SIZE + data_length]

                # Check if file complete.
                if data_length == 0:
                    break

                # Write data.
                f_out.write(data)

        print("[Success] Receive File OK")

    '''
    File transmission over Reliable Channel
    '''
    def send_file_reliable_channel(self):
        file_size = os.path.getsize(self.filename)
        print("File Size: {}".format(file_size))
        with open(self.filename, "rb") as f_in:
            while True:
                # Pack data.
                data = f_in.read(MAX_DATA_LENGTH)
                data_length = len(data)
                data += b"\0" * (MAX_DATA_LENGTH - data_length) # Pad packet to 1024.

                # Generate dummy sequence number and checksum. (Just to make all headers consistent between channels.)
                seq = 0 & 0xffffffff
                checksum = 0 & 0xffffffffffffffff

                # Send packet.
                packet = checksum.to_bytes(8, "big") + seq.to_bytes(4, "big") + data_length.to_bytes(4, "big") + data
                self.clientSocket.send(packet)

                # Check if file complete.
                if data_length == 0:
                    break

        print("[Success] File Send OK")

    # ######## Helper #############
    # This is a complete code, no need to change the function!
    def get_one_packet(self):
        # server receives packet from client and vice versa
        if self.is_server:
            packet = self.ice_helper.get_n_bytes_raw(self.clientSocket, self.packet_size_client)
        else:
            packet = self.ice_helper.get_n_bytes_raw(self.clientSocket, self.packet_size_server)

        return packet

    @staticmethod
    # This is a complete code, no need to change the function!
    def _print_data_hex(data, delay=0.0):
        print('-----', len(data), '-------')
        print(data.hex())
        time.sleep(delay)

    # This is a complete code, no need to change the function!
    def terminate(self):
        try:
            self.clientSocket.shutdown(SHUT_RDWR)
            self.clientSocket.close()
        except OSError:
            # the socket may be already closed
            pass